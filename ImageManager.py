"""
    ImageManager.py
    Author: Chris Kalas
    Date: 10/12/17

    This class keeps a history of operations on an image.

"""

import cv2 
import numpy as np 
import matplotlib.pyplot as plt
import os

OUTPUT = 'output'

class ImageManager():
    """A convenience class to save the steps of image processing. 
    """
    def __init__(self):
        self.image_history = {}
        self.function_history = ['original']
        self.path = os.path.join(os.getcwd(), OUTPUT)
        self.name = ''

        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def pull(self, name):
        """Return np.array

           A copy of the specified image from history.
        """
        return self.image_history[name].copy()

    def view(self, name):
        """Return np.arrayy

           A view of the specified image from history.
        """
        return self.image_history[name]

    def read(self, name):
        """Return None

            Read in the image and resize if too large.
        """

        # check for empty file
        if os.stat(name).st_size == 0:
            return False

        image = cv2.imread(name)
        if image.shape[0] != 400:
            image = cv2.resize(image, (532,400))
        
        self.image_history['original'] = image
        self.name = name.split(os.path.sep)[-1]

        return True


    def apply(self, func, *args, name=''):
        """Return None


            Apply a function to an image and store the resulting image and operation name.
        """
        self.function_history.append(name)
        self.image_history[name] = func(*args)

    def add(self,image, func_name, name=''):
        """Return None

            Add an image to this history which was not created using the apply() method.
        """
        self.image_history[name] = image.copy()
        self.function_history.append(name)

    def show_all(self):
        """Return None

           A blocking matplotlib subplot arrangement of all the image operations stored
           by the manager.
        """

        n_images = len(self.image_history)

        fig = plt.figure()
        rows = round(n_images/4 + 0.5)

        plt.subplot(rows, 4, 1)

        for i in range(n_images):
            fn = self.function_history[i]
            plt.subplot(rows, 4, i+1)
            plt.imshow(self.image_history[fn], cmap='gray')

            plt.xticks([])
            plt.yticks([])

        plt.show()

    def save_all(self):
        """Return None

           Save all the image operatinos stored by the manager.
        """
        for i in range(len(self.image_history)):
            fn = self.function_history[i]
            filename = os.path.join(self.path +'_all', self.name[:-4] + str(i) + '_' + fn + '.png')
            cv2.imwrite(filename, self.image_history[fn])

    def save_output(self):
        """Return None

           Save the final image stored in the manager.
        """
        filename = os.path.join(self.path+'_result', 'RESULT_' + self.name[:-4] + '.png')
        cv2.imwrite(filename, self.image_history[self.function_history[-1]])

