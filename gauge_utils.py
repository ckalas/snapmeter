import cv2
import numpy as np
from configparser import SafeConfigParser
from line_intersect import line_intersect_square

from math import sqrt, atan2

# config parser in global namespace
p = SafeConfigParser()
p.read('config.ini')

if not len(p):
    print('Config file "config.ini" not found, aborting.')
    exit(1)


def param(option):
    return p.getint('params', option)


def dbg(option):
    return p.getboolean('dbg', option)


def detect_circle(m):
    """ Return mask (np.array), drawing (np.array)

        Performs the hough circle transform and keep the closest circle
        to the centre of the image.
    """

    circles = cv2.HoughCircles(m.view('thresh'), cv2.HOUGH_GRADIENT, 5, 40, minRadius=10, maxRadius=40)
    drawing = m.pull('original')
    mask = np.zeros_like(drawing)
    h, w, _ = m.view('original').shape

    cx, cy = w//2, h//2

    min_distance = 1000
    min_index = -1
    cc = None
    if circles is not None:
        circles = np.uint16(np.around(circles))
        # calculate circle closest to image centre
        for n,i in enumerate(circles[0,:]):
            dist = sqrt((cx-i[0])**2 + (cy-i[1])**2)

            if dist < min_distance:
                min_distance = dist
                min_index = n

        # draw most central circle
        cc = circles[0, min_index]
        cv2.circle(drawing, (cc[0], cc[1]), cc[2], (0, 255, 0), 5)
        cv2.circle(mask, (cc[0], cc[1]), cc[2], (255, 255, 255), -1)
        # draw the center of the circle
        cv2.circle(drawing, (cc[0], cc[1]), 2, (0, 0, 255), 5)

        if dbg('CIRCLES'):
            # visualize all circles found
            for i in circles[0, :]:
                # draw the outer circle
                cv2.circle(drawing, (i[0], i[1]), i[2], (0, 255, 0), 5)
                # draw the center of the c ircle
                cv2.circle(drawing, (i[0], i[1]), 2, (0, 0, 255), 5)
    else:
        print('No circles found')

    return (mask, drawing, cc)


def too_close_to_border(x1, x2, y1, y2, w, h):

        angle = (180/np.pi)*atan2((y2-y1), (x2-x1))

        def check_angles_x(angle):
            for a in [90, 270, 45, 135]:
                print(angle, a, abs(angle-a))
                return (abs(abs(angle)-a) < param('THRESH_AX'))
            return False

        def check_angles_y(angle):
            for a in [0, 180, 360, 45, 135]:
                return (abs(abs(angle)-a) < param('THRESH_AY'))
            return False

        return (
               ((x1 < param('THRESH_X')) or (abs(w-x2) < param('THRESH_X')))
               and check_angles_x(angle)) or (((y1 < param('THRESH_Y')) or (abs(h-y2) < param('THRESH_Y')))
               and check_angles_y(angle))


def find_longest_and_closest_lines(lines, centre, w, h):
    """Return longest ([x1 x2 y1 y2]), closest([...])

       Calculate the longest and most central lines from
       hough line transform. Add a filter for lines that
       are too close to border, because those are misreads.

       Line must pass through square region in centre of image.
    """

    min_distance = 1000
    max_len = -1
    min_index_d = -1
    min_index_l = -1
    cx,cy = centre

    print(lines)

    xmin = w//8
    xmax = w - xmin
    ymin = h
    ymax = ymin - 200

    print('SQUARE', w,h,xmin,xmax,ymin,ymax)

    SQUARE = [
             [np.array([xmin,ymin]),np.array([xmax,ymin])],
             [np.array([xmin,ymin]),np.array([xmin,ymax])],
             [np.array([xmin,ymax]),np.array([xmax,ymax])],
             [np.array([xmax,ymax]),np.array([xmax,ymin])]
             ]



    for x in range(len(lines)):
        for x1,y1,x2,y2 in lines[x]:

            length = sqrt((x2-x1)**2 + (y2-y1)**2)
            dist1 = sqrt((cx-x1)**2 + (cy-y1)**2)
            dist2 = sqrt((cx-x2)**2 + (cy-y2)**2)
            dist = min(dist1,dist2)

            if dbg('POINTS'):
                canvas = np.zeros((cy*2,cx*2, 3))
                print((x1,y1),(x2,y2), (cx,cy))

                cv2.circle(canvas,(x1,y1),2,(0,0,255),5)
                cv2.circle(canvas,(x2,y2),2,(0,255,0),5)
                cv2.circle(canvas,(cx,cy),2,(255,0,255),5)

                cv2.imshow('',canvas)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            #if not line_intersect_square([np.array([x1,y1]), np.array([x2,y2])], SQUARE ):
            #    continue

            if too_close_to_border(x1,x2,y1,y2,w,h):
                continue

            if dist < min_distance:
                min_distance = dist
                min_index_d = x

            if dbg('ON'):
                print(length, max_len, dist)

            if length > max_len and dist < param('THRESH_DIST'):
                max_len = length
                min_index_l = x

    if min_index_d < 0 or min_index_l < 0:
        return None

    longest = lines[min_index_l][0] 
    closest = lines[min_index_d][0]

    return (longest, closest)

def hough_lines(m, cc):
    """Return drawing (np.array), angle (float)

       Calculates the hough transform and finds the line most
       likely to be the dial based on a decision rule.
    """


    drawing = m.pull('original') 


    h,w,_ = drawing.shape 

    # 6. probabalistic hough transform
    lines = cv2.HoughLinesP(m.pull('skeleton'),rho=1,theta=np.pi/180,threshold=50,minLineLength=50,maxLineGap=20)
    #cy,cx,_ = cc
    cy,cx = w//2,h//2

    #ccd = sqrt((cc[0]-cx)**2 + (cc[1]-cy)**2)
    #if ccd < param('THRESH_CC'):
    #    cy = cc[1]
    #    cx = cc[0]

    print(w,h, cx,cy)
    angle = None
    line = None

    if not lines is None:
        
        # 7. find longest line and line closest to centre
        ret = find_longest_and_closest_lines(lines, (cy, cx), w, h)
        if ret:
            longest, closest = ret
            lx1,ly1,lx2,ly2 = longest  
            cx1,cy1,cx2,cy2 = closest

            if dbg('LINES'):
                print('lmao')
                print(longest)
                print(closest)
                # longest line = pink  
                cv2.line(drawing,(lx1,ly1),(lx2,ly2),(255,0,255),7)
                # min distance to centre = aqua      
                cv2.line(drawing,(cx1,cy1),(cx2,cy2),(255,255,0),7)
                cv2.imshow('', drawing)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            # centre length and longest length
            cl = sqrt((cx2-cx1)**2 + (cy2-cy1)**2)
            ll = sqrt((lx2-lx1)**2 + (ly2-ly1)**2)
            ld = min(sqrt((cx-lx1)**2 + (cy-ly1)**2),sqrt((cx-lx2)**2 + (cy-ly2)**2))
            cd = min(sqrt((cx-cx1)**2 + (cy-cy1)**2),sqrt((cx-cx2)**2 + (cy-cy2)**2))
            print('CENTER: ', cx,cy)

            # 8. if distance from longest line to centre and closest is similar, longest is right
            #    if the center one is longer than longest, take centre
            print('DISTANCES: ', cd,ld,abs(cd-ld))
            if (cl < ll) and ((ld < cd) or abs(cd-ld) < 89) and (not too_close_to_border(lx1,lx2,ly1,ly2,w,h)):
                print("longest 1", ll, cl)     
                cv2.line(drawing,(lx1,ly1),(lx2,ly2),(0,0,255),10)
                angle = (180/np.pi)*atan2((ly2-ly1),(lx2-lx1))
                line = longest

            else:
                print("longest 2 ", cl, ll)      
                cv2.line(drawing,(cx1,cy1),(cx2,cy2),(0,0,255),10)
                # 9. angle
                angle = (180/np.pi)* atan2((cy2-cy1),(cx2-cx1))
                line = closest

            # find quadrant and offset angle
            x1,x2,y1,y2 = line
            dx = x1-cx
            dy = y1-y2

            # apply 90 degree offset to all angles so that 0 is the bottom of the image
            angle += 90

            # add 180 degree if dial is in right half of image (to make range 0-360)
            if (dx > 0):
                angle +=180

            print('Dial angle: ', angle)

        else:
            print('No lines found')

    return (drawing, angle)
