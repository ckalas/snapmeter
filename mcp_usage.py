import requests
import json
import pandas as pd
import matplotlib.pyplot as plt
from enum import Enum
from math import sin, cos, atan2, sqrt

API = 'https://api.snapmeter.io'
LOG_ENDPOINT = 'logs'

IGN = ['0x00072563', '0x00072db2', '0x00072beb', '0x00072859']

KEKANG = {'lat': 31.31110, 'lon': 121.6007}

def value_to_fill_status(value):

    if value < 1:
        return '0_EMPTY'
    elif value < 17.5:
        return '1_IN_USE'
    else:
        return '2_FULL'


def aggregate_daily_status(data):

    data = data.groupby(['nodeID','timestamp']).last()
    data.index = data.index.droplevel(0)
    data['status'] = data['value'].apply(value_to_fill_status).astype('category')
    print(data)
    data = data.groupby(['timestamp','status']).count()
    data['value'].unstack(fill_value=0).plot.bar(stacked=True)
    print(data['value'].unstack(fill_value=0))

def distance(lat1, lon1):

    lat2, lon2 = KEKANG['lat'], KEKANG['lon']
    dlon = lon2 - float(lon1)
    dlat = lat2 - float(lat1)

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return 6373.0 * c


if __name__ =='__main__':
    # fetch data
    req = requests.get(f'{API}/{LOG_ENDPOINT}')
    jdata = json.loads(req.content)

    # process
    data = pd.DataFrame(jdata)
    data.columns = ['lat','lon', 'nodeID', 'timestamp', 'value']

    # todo: filter only those near customer site
    data = data.drop(data[data.value > 250].index)
    data = data.drop(data[data.lat <= 0].index)

    data['distance'] = data[['lat','lon']].apply(lambda x: distance(*x), axis=1)
    print(data.head)
    data = data[~data.nodeID.isin(IGN)]
    data = data.drop(data[data.distance > 20].index)
    data = data.drop(columns=['lat', 'lon', 'distance'])

    # TODO: somehow only keep the last reading on each day?
    data['timestamp'] = pd.to_datetime(data['timestamp'])
    data['timestamp'] = data['timestamp'].dt.date

    # calculate stuff and plot it
    aggregate_daily_status(data)
    

    # show all plots
    plt.show()

