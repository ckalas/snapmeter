import numpy as np


def intersects(a, b):
    p = [b[0][0]-a[0][0], b[0][1]-a[0][1]]
    q = [a[1][0]-a[0][0], a[1][1]-a[0][1]]
    r = [b[1][0]-b[0][0], b[1][1]-b[0][1]]

    t = (q[1]*p[0] - q[0]*p[1])/(q[0]*r[1] - q[1]*r[0]) \
        if (q[0]*r[1] - q[1]*r[0]) != 0 \
        else (q[1]*p[0] - q[0]*p[1])
    u = (p[0] + t*r[0])/q[0] \
        if q[0] != 0 \
        else (p[1] + t*r[1])/q[1]

    return t >= 0 and t <= 1 and u >= 0 and u <= 1


def line_intersect_square(line, square):
    a1, a2 = line
    for side in square:
        intersect = intersects(line, [side[0],side[1]])

        if intersect:
            print('GOOD LINE: ', line)
            return True

    return False


if __name__ == '__main__':

    import matplotlib.pyplot as plt
    square = [
              [np.array([350, 133]), np.array([350, 399])],
              [np.array([350, 133]), np.array([150, 133])],
              [np.array([150, 133]), np.array([150, 399])],
              [np.array([150, 399]), np.array([350, 399])]
              ]

    lines = [[np.array([10, 264]), np.array([250, 362])]]

    for side in square:
        p1, p2 = side
        plt.plot([p1[0], p2[0]], [p1[1], p2[1]])

    for line in lines:
        plt.plot([line[0][0], line[1][0]], [line[0][1], line[1][1]])
        print(line_intersect_square(line, square))

    plt.show()
