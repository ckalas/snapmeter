"""
    read_gauge.py
    Author: Chris Kalas
    Date: 14/12/17

    This script extracts the angle of the dial on a pressure gauge.
    The following assumptions are made:

    The image is taken in such a way that the gauge is vertically aligned
    with the centre of the dial being a close to the centre of the image
    as possible. Decent illumination provided. Typically depending the best
    working distance is between 20cm and 40cm from the lens to the front of
    dial.

    The algorithm is as follows:

    1. convert image to grayscale
    2. apply gaussian blur
    3. run an adaptive threshold (NOTE: this leaves the needle and gauge black
       with a white background - this has to be inverted later for the hough
       transform)
    4. an inversion is applied
    5. the image is skeletonized (reducing everything to sinle pixel width)
    6. probablistic hough line transform is applied
    7. the longest line and line closest to image center are kept
    8. if min distance to centre is shortest, its wrong, the longest line is
       better
    9. calculate angle of the line and return

"""


import cv2
import numpy as np
from ImageManager import ImageManager
from gauge_utils import *
import sys
import os
from skimage.morphology import skeletonize


if __name__ == '__main__':

    # if multiple files are included then dont display all, just save outputs
    batch = False

    if len(sys.argv) == 2:
        arg = sys.argv[1]
        if os.path.isdir(arg):
            batch = True
            ims = os.listdir(arg)

            ims = [os.path.join(os.getcwd(), arg, im) for im in ims]
        else:
            ims = [sys.argv[1]]

    elif len(sys.argv) > 2:
        ims = sys.argv[1:]
        flag = True

    else:
        print('Usage: python3 read_gauge.py <img1 img2 ... >')

    ims.sort()
    for im in ims:
        # keep track of images operations performed and visualize them together
        m = ImageManager()

        # automatic resizing
        if im.split(os.path.sep)[-1][0] == '.':
            continue
        print(im)
        if not (m.read(im)):
            continue
        # 1. grayscale
        m.apply(cv2.cvtColor, m.pull('original'), cv2.COLOR_BGR2GRAY, name='gray')
        # 2. gaussian blur
        m.apply(cv2.blur,m.pull('gray'),(9,9), name='blur')
        # 3. adaptive threshold
        m.apply(cv2.adaptiveThreshold, m.pull('blur'),255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,35,2, name='thresh') 
        # 4a. invert threshold image
        inv = np.invert(m.pull('thresh'))
        # 4b. rescale the image as required by skeletonization
        inv[np.where(inv == 255)] = 1
        # 5. skeletonize
        skeleton = np.array(skeletonize(inv)*255, dtype=np.uint8)

        m.add(skeleton, 'skeleton', name='skeleton')

        mask, drawing, cc = detect_circle(m)
        m.add(drawing, 'cirlce', name='circle')

        drawing, angle = hough_lines(m, cc)

        m.add(drawing, 'hough_lines', name='lines_%.2f' % (angle) if angle else 'nolines')

        if dbg('SAVE_OUTPUTS_ONLY'):
            m.save_output()
        else:
            m.save_all()

        if not batch:
            m.show_all()
