import sys
import os
import re
from collections import defaultdict
from pymongo import MongoClient
from datetime import datetime

"""
log = {
        'nodeID': ,
        'messageID': ,
        'reading': {
            'timestamp': ,
            'lat':,
            'long',
            'value'
        }
}
"""

def log_to_mongo(input_file):
    """
        log = {
                'nodeID': ,
                'messageID': ,
                'reading': {
                    'timestamp': ,
                    'lat':,
                    'long',
                    'value'
                }
        }
    """
    # Create connection to MongoDB
    client = MongoClient('localhost', 27017)
    db = client['snapmeter_dev']
    collection = db['logs']

    data = defaultdict(list)
    with open(input_file, 'r+') as f:

        delimiters = "\n\n", "\n \n \n"
        regex= '|'.join(map(re.escape, delimiters))
        messages = f.read()
        messages = re.split(regex, messages)
        print(f"Found {len(messages)} messages")
    
        for message in messages:
            messageId = re.findall(r'MessageId:(.*)', message)
            nodeId = re.findall(r'nodeId:(0x\w{8})', message)
            latlon = re.findall(r'lat,lon: (\d{1,3}\.\d{6}),(\d{1,3}\.\d{6})', message)
            payload = re.findall(r'payload: (.*)', message)
            payload_noloc = re.findall(r'Converted\(No Location\):\n(.*)', message)

            #big = re.findall(r'nodeId:(0x\w{8})|lat,lon: (\d{1,3}\.\d{6}),(\d{1,3}\.\d{6})|payload: (.*)|Converted\(No Location\):\n(.*)', message)

            timedate = re.findall(r'Time:\s+(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2})', message)


            

            if len(nodeId):
                tmp = []
                if len(payload):
                    tmp = payload
                elif len(payload_noloc):
                    tmp = payload_noloc
                else:
                    continue

                if not len(latlon):
                    latlon = [(-1,-1)]

                try:
                    for value in tmp[0].split()[1::2]:
                        collection.insert({
                                        'nodeID': nodeId[0],
                                        'messageID': messageId[0],
                                        'reading': {
                                                    'timestamp':datetime.strptime(f"{timedate[0][0]}T{timedate[0][1]}", "%Y-%m-%dT%H:%M:%S"),
                                                    'lat': float(latlon[0][0]),
                                                    'lon': float(latlon[0][1]),
                                                    'value': int(value)
                                                   }
                                          }
                                         )

                except Exception as e:
                    print(e,messageId)
                    


def parse_file(input_file):
    data = defaultdict(list)
    with open(input_file, 'r+') as f:

        delimiters = "\n\n", "\n \n \n"
        regex= '|'.join(map(re.escape, delimiters))
        messages = f.read()
        messages = re.split(regex, messages)
        print(f"Found {len(messages)} messages")
    
        for message in messages:
            nodeId = re.findall(r'nodeId:(0x\w{8})', message)
            latlon = re.findall(r'lat,lon: (\d{1,3}\.\d{6}),(\d{1,3}\.\d{6})', message)
            payload = re.findall(r'payload: (.*)', message)
            payload_noloc = re.findall(r'Converted\(No Location\):\n(.*)', message)

            #big = re.findall(r'nodeId:(0x\w{8})|lat,lon: (\d{1,3}\.\d{6}),(\d{1,3}\.\d{6})|payload: (.*)|Converted\(No Location\):\n(.*)', message)

            timedate = re.findall(r'Time:\s+(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2})', message)

            if len(nodeId):
                tmp = []
                if len(payload):
                    tmp = payload

                elif len(payload_noloc):
                    tmp = payload_noloc
                else:
                    continue

                try:
                    values = tmp[0].split()[1::2]
                    data[nodeId[0]] += values
                except Exception as e:
                    print(e)

    return data


def data_to_csv(data):
    with open('log.csv', 'w+') as f:
        for key in data.keys():
            tmp = str(data[key])[1:-1].replace(' ', '')
            print(f"{key},{tmp}", file=f)


if __name__ == '__main__':

    input_file = ''

    if not len(sys.argv) > 1:
        print('Usage: python3 logs_to_csv.py <logfile>')
        exit(1)
    else:
        input_file = sys.argv[1]
        if not os.path.exists(input_file):
            print('Invalid file')
            exit(2)

    log_to_mongo(input_file)
    data = parse_file(input_file)
    data_to_csv(data)

