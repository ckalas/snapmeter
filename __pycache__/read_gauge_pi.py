import cv2 
import numpy as np 
import picamera


from skimage.morphology import skeletonize
from skimage import data
#import matplotlib.pyplot as plt
from math import sqrt, atan2
import subprocess

from subprocess import check_output as qx
qx(['sudo', 'hcitool', '-i hci0 cmd 0x08 0x0008 1E 02 01 1A 1A FF 4C 00 02 15 C7 CC 33 1c 26 A2 44 DC 88 6E 53 84 4C B4 09 A8 00 00 00 %2x C8' % int(angle)])

DEBUG = False

def hough_lines(img, n_lines):
    inv = np.invert(img)
    inv[np.where(inv == 255)] = 1

    skeleton = np.array(skeletonize(inv)*255, dtype=np.uint8)

    #drawing = m.pull('original')
    #t = cv2.convertScaleAbs(skeleton)
    #m.add(skeleton, 'skeleton', name='skeleton')

   
    lines = cv2.HoughLinesP(skeleton,rho=1,theta=np.pi/180,threshold=50,minLineLength=50,maxLineGap=20)

    min_distance = 1000
    max_len = -1
    min_index_d = -1
    min_index_l = -1

    w,h = img.shape
    cy,cx = w//2,h//2

    if lines != None:

        for x in range(len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                length = sqrt((x2-x1)**2 + (y2-y1)**2)
                dist = sqrt((cx-x1)**2 + (cy-y1)**2)

                if dist < min_distance:
                    min_distance = dist
                    min_index_d = x

                if length > max_len:
                    max_len = length
                    min_index_l = x



        # CRITERIA: if min distance to centre is shortest, its wrong
   

        # longest line = pink
        longest = lines[min_index_l][0] 
        lx1,ly1,lx2,ly2 = longest  

        #if DEBUG:     
        #    cv2.line(drawing,(lx1,ly1),(lx2,ly2),(255,0,255),7)


        # min distance to centre = aqua
        closest = lines[min_index_d][0]
        cx1,cy1,cx2,cy2 = closest

        #if DEBUG:      
        #    cv2.line(drawing,(cx1,cy1),(cx2,cy2),(255,255,0),7)

        cl = sqrt((cx2-cx1)**2 + (cy2-cy1)**2) 
        ll = sqrt((lx2-lx1)**2 + (ly2-ly1)**2)
        angle = None
        line = None

        # x1 < x2, y1 > y2

        # find quadrant:

        if cl < ll:      
            cv2.line(drawing,(lx1,ly1),(lx2,ly2),(0,0,255),10)
            print(lx1,lx2,ly1,ly2)
            angle = (180/np.pi)*atan2((ly2-ly1),(lx2-lx1))
            line = longest
        else:      
            cv2.line(drawing,(cx1,cy1),(cx2,cy2),(0,0,255),10)
            print(lx1,lx2,ly1,ly2)
            angle = (180/np.pi)* atan2((cy2-cy1),(cx2-cx1))
            line = closest

        # find quadrant and offset angle
        x1,x2,y1,y2 = line
        dx = x1-cx
        dy = y1-y2

        if (dx < 0):
            angle +=90
        else:
            angle += 270


        print('Dial angle: ', angle)
    else:
        print('No lines found oops')




    return (drawing, angle)

if __name__ =='__main__':

cmd = 'sudo hciconfig'
qx([cmd, 'hci0 up'])
qx([cmd, 'hci0 leadv 3'])
    while(1):
        # Create the in-memory stream
        print('1')
        stream = io.BytesIO()
        with picamera.PiCamera() as camera:
            camera.start_preview()
            time.sleep(2)
            camera.capture(stream, format='jpg', resize=(400, 532))

        print('2')
        # Construct a numpy array from the stream
        data = np.fromstring(stream.getvalue(), dtype=np.uint8)
        # "Decode" the image from the array, preserving colour
        image = cv2.imdecode(data, 1)
        # OpenCV returns an array with data in BGR order. If you want RGB instead
        # use the following...
        image = image[:, :, ::-1]

        print('3')
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.blur(gray,(9,9))

        thresh = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,35,2)
        drawing,angle = hough_lines(m, N_LINES)
        print('4')
        cmd_str = 'sudo hcitool -i hci0 cmd 0x08 0x0008 1E 02 01 1A 1A FF 4C 00 02 15 C7 CC 33 1c 26 A2 44 DC 88 6E 53 84 4C B4 09 A8 00 00 00 %2x C8' % int(angle)

        qx(cmd_str.split())

        print('5')

