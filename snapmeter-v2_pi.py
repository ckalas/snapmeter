import io
import time 
import cv2 
import os
import numpy as np
import picamera
from ConfigParser import SafeConfigParser
from skimage import data
#import matplotlib.pyplot as plt
from math import sqrt, atan2
from subprocess import check_output as qx
from skimage.morphology import skeletonize

import time
import serial


ser = serial.Serial(
  
   port='/dev/ttyAMA0',
   baudrate = 115200,
   parity=serial.PARITY_NONE,
   stopbits=serial.STOPBITS_ONE,
   bytesize=serial.EIGHTBITS,
   timeout=1
)


# config parser in global namespace
p = SafeConfigParser()
p.read('config.ini')

if not p:
    print('Config file "config.ini" not found, aborting.')
    exit(1)

param = lambda option: p.getint('params', option)
dbg = lambda option: p.getboolean('dbg', option)
cfg = lambda option: p.getboolean('cfg', option)


def find_longest_and_closest_lines(lines,centre):
    """Return longest ([x1 x2 y1 y2]), closest([...])

       Calculate the longest and most central lines from
       hough line transform. Add a filter for lines that
       are too close to border, because those are misreads.
    """

    min_distance = 1000
    max_len = -1
    min_index_d = -1
    min_index_l = -1
    cx,cy = centre


    for x in range(len(lines)):
        for x1,y1,x2,y2 in lines[x]:

            length = sqrt((x2-x1)**2 + (y2-y1)**2)
            dist1 = sqrt((cx-x1)**2 + (cy-y1)**2)
            dist2 = sqrt((cx-x2)**2 + (cy-y2)**2)
            dist = min(dist1,dist1)

            if dbg('POINTS'):
                canvas = np.zeros((cy*2,cx*2, 3))
                print((x1,y1),(x2,y2), (cx,cy))

                cv2.circle(canvas,(x1,y1),2,(0,0,255),5)
                cv2.circle(canvas,(x2,y2),2,(0,255,0),5)
                cv2.circle(canvas,(cx,cy),2,(255,0,255),5)

                cv2.imshow('',canvas)
                cv2.waitKey(0)
                cv2.destroyAllWindows()


            if dist < min_distance:
                min_distance = dist
                min_index_d = x

            if dbg('ON'):
                print(length, max_len, dist)

            if length > max_len and dist < param('THRESH_DIST'):
                max_len = length
                min_index_l = x

    longest = lines[min_index_l][0]  

    closest = lines[min_index_d][0]

    return (longest, closest)

def hough_lines(img):
    """Return drawing (np.array), angle (float)

       Calculates the hough transform and finds the line most
       likely to be the dial based on a decision rule.
    """


    def too_close_to_border(x1,x2,y1,y2, w,h):

        angle = (180/np.pi)*atan2((y2-y1),(x2-x1))

        def check_angles_x(angle):
            for a in [90, 270, 45, 135]:
                print(angle, a,abs(angle-a) )
                return (abs(abs(angle)-a) < param('THRESH_AX'))
            return False

        def check_angles_y(angle):
            for a in [0, 180, 360,45, 135]:
                return (abs(abs(angle)-a) < param('THRESH_AY'))
            return False

        return (((x1 < param('THRESH_X')) or (abs(w-x2) < param('THRESH_X'))) and check_angles_x(angle)) or (((y1 < param('THRESH_Y') ) or (abs(h-y2) < param('THRESH_Y'))) and check_angles_y(angle))


    inv = np.invert(img)
    inv[np.where(inv == 255)] = 1

    skeleton = np.array(skeletonize(inv)*255, dtype=np.uint8)

    lines = cv2.HoughLinesP(skeleton,rho=1,theta=np.pi/180,threshold=50,minLineLength=50,maxLineGap=20)

    min_distance = 1000
    max_len = -1
    min_index_d = -1
    min_index_l = -1

    w,h = img.shape
    cy,cx = w//2,h//2
    angle = 0

    if not lines is None:

        longest, closest = find_longest_and_closest_lines(lines,(cy,cx))
        lx1,ly1,lx2,ly2 = longest  
        cx1,cy1,cx2,cy2 = closest


        cl = sqrt((cx2-cx1)**2 + (cy2-cy1)**2) 
        ll = sqrt((lx2-lx1)**2 + (ly2-ly1)**2)
        line = None

        if dbg('ON'):
                print(lx1,lx2,ly1,ly2,cx1,cx2,cy1,cy2)

        # x1 < x2, y1 > y2

        # find quadrant:

        if cl < ll:      
            #cv2.line(drawing,(lx1,ly1),(lx2,ly2),(0,0,255),10)
            angle = (180/np.pi)*atan2((ly2-ly1),(lx2-lx1))
            line = longest
        else:      
            #cv2.line(drawing,(cx1,cy1),(cx2,cy2),(0,0,255),10)

            angle = (180/np.pi)* atan2((cy2-cy1),(cx2-cx1))
            line = closest

        # find quadrant and offset angle
        x1,x2,y1,y2 = line
        dx = x1-cx
        dy = y1-y2

        if (dx < 0):
            angle +=90
        else:
            angle += 180


        print('Dial angle: ', angle)
    else:
        print('No lines found oops')


    return (angle)

if __name__ =='__main__':

    IMG_DIR = os.path.join('/home','pi','images')
    # BACKUP = os.path.join('/home', 'pi', 'backup')

    if not os.path.exists(IMG_DIR):
        os.mkdir(IMG_DIR)

    if cfg('BLUETOOTH'):
        try:
            qx(['sudo', 'hciconfig', 'hci0', 'up'])
            qx(['sudo', 'hciconfig', 'hci0', 'leadv', '3'])
        except Exception as e:
            print('Cannot setup bluetooth, assuming it is already running')
        

    for _ in xrange(5):
        # Create the in-memory stream
        stream = io.BytesIO()
        with picamera.PiCamera() as camera:
            camera.start_preview()
            time.sleep(2)
            camera.capture(stream, format='jpeg', resize=(532, 400))

        # Construct a numpy array from the stream
        data = np.fromstring(stream.getvalue(), dtype=np.uint8)
        # "Decode" the image from the array, preserving colour
        image = cv2.imdecode(data, 1)


        cv2.imwrite(os.path.join(IMG_DIR,'img-%d.jpg' % len(os.listdir(IMG_DIR))), image)

        print('Captured image...')
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.blur(gray,(9,9))

        thresh = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,35,2)
        angle = hough_lines(thresh)
        pressure = (angle - 45) * (400/270)

        if _ == 4:
            ser.write('%d\r' % (angle))
        else:
            ser.write('%d\n' % (angle))

        print('Image processed...')

        if cfg('BLUETOOTH'):
            cmd_str = 'sudo hcitool -i hci0 cmd 0x08 0x0008 1E 02 01 1A 1A FF 4C 00 02 15 C7 CC 33 1c 26 A2 44 DC 88 6E 53 84 4C B4 09 A8 00 00 00 %2x C8' % int(angle)
            qx(cmd_str.split())

        print('End loop')
        print('Sleeping for 28 seconds...')
        time.sleep(28)

