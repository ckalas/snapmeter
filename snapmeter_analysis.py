import pandas as pd
from kshape.core import kshape, zscore
import matplotlib.pyplot as plt 

df = pd.read_csv('snap.csv', usecols=['nodeID', 'value', 'timestamp'])

print(df.head())

# aggregate values by node id

grouped_nodes = df.groupby('nodeID')


time_series = []
nodes = []

N = 80
K = 4

for group in grouped_nodes:
    values = group[1]['value'].tolist()
    if len(values) > N:
        nodes.append(group[0])
        time_series.append(values[:N])

clusters = kshape(zscore(time_series, axis=1), K)

print(f'{len(nodes)} nodes clustered')

for cluster in clusters:
    print(f'Cluster: {[nodes[x] for x in cluster[1]]}')
 #   plt.scatter(cluster[0][2], cluster[0][3])

#plt.show()
