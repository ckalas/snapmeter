#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <boost/range/iterator_range.hpp>
#include <boost/filesystem.hpp>


namespace fs = boost::filesystem;

using namespace cv;
using namespace std;
using namespace fs;

bool operator ! (const Mat&m) { return m.empty(); }

tuple<Mat, Mat> find_longest_and_closest_lines(Mat lines, Size size, int *ok);
double distance(double x1, double x2, double y1, double y2);
bool too_close_to_border(int x1, int x2, int y1, int y2, int w, int h);
double read_gauge(String input, int *name_counter);
void validate_angle(int angle);

int main(int argc, char **argv) {
    cout << "Snapmeter" << endl;
    
    if (argc < 1) {
        return 1;
    }

    path p(argv[1]);

    auto isDir = is_directory(p);
    auto isFile = is_regular_file(p);
    double angle;

    if (!(isDir | isFile)) {
        cout << "Invalid input. Ensure absolute path is provided" << endl;
    }


    // todo: handle directory/file input better
    int name_counter = 0;
    if (isDir) {
        for(auto& entry : boost::make_iterator_range(directory_iterator(p.string()), {})) {
            cout << entry.path().string() << endl;
            angle = read_gauge(entry.path().string(), &name_counter);
            validate_angle(angle);

            
        }
    }

    else {
        angle = read_gauge(p.string(), &name_counter);
        validate_angle(angle);
     
    }

    

    return 0;
}

void validate_angle(int angle) {
    if (angle == -1) {
        cout << "Invalid angle" << endl;
    }
    else {
        cout << "Angle: " << angle << endl;
    }
}
double read_gauge(String input, int *name_counter) {
    Mat image, proc;
    int w, h, cx, cy;

    image = imread(input);

    Size imageSize = image.size();
    w = imageSize.width;
    h = imageSize.height;
    ostringstream output_name;

    if (!(w|h)) return -1;

    cx = ceil(h/2);
    cy = ceil(w/2);

    //cout << imageSize << endl;
    // Process
    cvtColor(image, proc, COLOR_BGR2GRAY);
    blur(proc, proc, Size(9,9));
    adaptiveThreshold(proc, proc, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 35, 2);
 
    // Skeletonize
    Mat skel(imageSize, CV_8UC1, cv::Scalar(0));
    Mat temp(imageSize, CV_8UC1);
    Mat element = getStructuringElement(MORPH_CROSS, Size(3, 3));

    bool done;
    do {
        morphologyEx(proc, temp, cv::MORPH_OPEN, element);
        bitwise_not(temp, temp);
        bitwise_and(proc, temp, temp);
        bitwise_or(skel, temp, skel);
        erode(proc, proc, element);
        
        double max;
        cv::minMaxLoc(proc, 0, &max);
        done = (max == 0);
    } while (!done);

    // Hough lines
    Mat lines, longest, closest, line;
    double angle;
    tuple<Mat,Mat> longest_closest;

    HoughLinesP(skel, lines,1, CV_PI/180, 50, 50, 10);

    // no lines found
    if (!lines) {
        
        return -1;
    }
    else {
        int ok = 0;
        longest_closest = find_longest_and_closest_lines(lines, imageSize, &ok);

        if(!ok) {
            return -1;
        }

        longest = get<0>(longest_closest);

        closest = get<1>(longest_closest);
        int lx1,lx2,ly1,ly2,cx1,cx2,cy1,cy2;
        double cl,ll, cd, ld;

        cx1 = closest.at<int>(0);
        cx2 = closest.at<int>(2);
        cy1 = closest.at<int>(1);
        cy2 = closest.at<int>(3);

        lx1 = longest.at<int>(0);
        lx2 = longest.at<int>(2);
        ly1 = longest.at<int>(1);
        ly2 = longest.at<int>(3);

        cl = distance(cx1,cx2,cy1,cy2);
        ll = distance(lx1,lx2,ly1,ly2);
        ld = min(distance(cx,lx1,cy,ly1),distance(cx,lx2,cy,ly2));
        cd = min(distance(cx,cx1,cy,cy1),distance(cx,cx2,cy,cy2));
        
        if ( (cl < ll) && ((ld < cd) || (abs(cd-ld) < 89))) {
            angle = (180/M_PI)*atan2((ly2-ly1),(lx2-lx1));
            line = longest;
            cv::line(image, Point(lx1,ly1), Point(lx2,ly2), Scalar(0,0,255), 10);

        }

        else {
            cout << closest << longest << " THIS IS FAKE" << endl;
            angle = (180/M_PI)* atan2((cy2-cy1),(cx2-cx1));
            line = closest;
            cv::line(image, Point(cx1,cy1), Point(cx2,cy2), Scalar(255,0,255), 10);
            //cv::line(image, Point(lx1,ly1), Point(lx2,ly2), Scalar(0,255,255), 10);
        }

        // todo: add offsets
    }

    output_name << "../output/" << *name_counter << ".png";
    imwrite(output_name.str(), image);
    (*name_counter)++;
    return angle;
    
}

double distance(double x1, double x2, double y1, double y2) {
    return sqrt(pow((x2-x1),2) + pow((y2-y1),2));
}

bool too_close_to_border(int x1, int x2, int y1, int y2, int w, int h) {
    //auto angle = (180/M_PI) * atan2(y2-y1, x2-x1);
    // todo: check angle ?
    return (x1 < 20) | (abs(w-x2) < 20) |(y1 < 10) | (abs(h-y2) < 10);
    }

    tuple<Mat, Mat> find_longest_and_closest_lines(Mat lines, Size size, int *ok) {
    // todo: cy and cx might be backwards oops
    int w,h,cx,cy, min_distance, max_len, min_index_d, max_index_l;
    w = size.width;
    h = size.height;
    cy = ceil(h/2);
    cx = ceil(w/2);
    min_distance = 1000;
    max_len = -1;
    min_index_d = -1;
    max_index_l = -1;

    Mat m;
    for(auto i=0; i < lines.rows; i++) {
        int x1,y1,x2,y2;
        double length, dist1, dist2, dist;
        auto row = lines.row(i);

        x1 = row.at<int>(0);
        x2 = row.at<int>(2);
        y1 = row.at<int>(1);
        y2 = row.at<int>(3);

        length = distance(x1,x2,y1,y2);
        dist1 = distance(x1,cx,y1,cy); 
        dist2 = distance(x2,cx,y2,cy); 
        dist = min(dist1,dist2);
        //cout << length << " " << dist1 << " " << dist2 << " " << dist << endl;

        if (too_close_to_border(x1,x2,y1,y2,w,h)) {
            //cout << "Too close to  border" << endl;
            continue;
        }

        if (dist < min_distance) {
            min_distance = dist;
            min_index_d = i;
        }

        if (length > max_len) {
            max_len = length;
            max_index_l = i;
        }
    }

    if (min_index_d >= 0 && max_index_l >= 0) {
        *ok = 1;
        return tuple<Mat,Mat>(lines.row(max_index_l), lines.row(min_index_d));
    }

    else {
        
        return tuple<Mat,Mat>(m,m);
    }

}

