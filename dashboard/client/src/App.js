import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import "./App.css";
import "mapbox-gl/dist/mapbox-gl.css";
import { Header, Icon, Grid, Menu } from "semantic-ui-react";
import TableView from "./components/TableView/TableView";
import MapView from "./components/MapView/MapView";



// TODO: extract stuff to functional comps yo

const HeaderBar = props => {
  let numberOfDevices = props.numberOfDevices;
  let deviceString = numberOfDevices > -1 ? `${numberOfDevices} devices` : "";

  return (
    <Header as="h2" className="header-bar">
      <Icon name="tags" id="device-count" />
      <Header.Content id="device-count-text">{deviceString}</Header.Content>
    </Header>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { numberOfDevices: -1 };
  }


  componentDidMount() {
    const options = {
      method: "GET",
      headers: {
        "Access-Control-Request-Headers": "GET",
        "Authorization": 'Basic bGluZGU6TGluZGUxMjM0'
      }
    };


    fetch("https://api.snapmeter.io/logs/devices/count", { options })
      .then(res => res.json())
      .then(json => {
        this.setState({ numberOfDevices: json[0].numberOfDevices });
      });
  }


  render() {
    return (
      <Router>
        
        <div className="App">
        
          <header className="App-header">
            <Grid columns="three" verticalAlign="middle">
              <Grid.Column>
                <HeaderBar numberOfDevices={this.state.numberOfDevices} />
              </Grid.Column>
              <Grid.Column>
                <h1 id="title">Snapmeter</h1>
              </Grid.Column>
              <Grid.Column>
                  <Menu compact floated='right' id='icon-menu'>
                    <Menu.Item name='table'>
                      <Link to='/'>
                        <Icon name="table" id="table-link" />
                      </Link>
                    </Menu.Item>
                    
                      <Menu.Item name='map'>
                        <Link to='/map'>
                          <Icon name="map marker alternate" id="map-link" />
                        </Link>
                      </Menu.Item>
                  </Menu>
              </Grid.Column>
            </Grid>
          </header>

          <div className="App-body">
            <Route exact path="/" component={TableView} />
            <Route path="/map" component={MapView} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
