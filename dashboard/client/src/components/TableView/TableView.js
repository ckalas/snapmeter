import _ from "lodash";
import React, { Component } from "react";
import Loader from "react-loader-spinner";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { Dropdown, Icon, Button } from "semantic-ui-react";
import Plot from "react-plotly.js";
import ReactModal from "react-modal";
import { saveAs } from "file-saver/FileSaver";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

var moment = require('moment')
require('moment-timezone');


const PI_IDS = [
    "0x00072beb",
    "0x00072db2",
    "0x00072563",
    "0x00072859"
];

const CURRENT_IDS = [
    "0x000c0aba",
    "0x000c0861",
    "0x000c071c",
    "0x000c08a5",
    "0x000c08a7",
    "0x000c090a",
    "0x000c08b2",
    "0x000c0917",
    "0x000c07ff",
    "0x000c086a",
    "0x000c0908",
    "0x000c0917",
    "0x0000c088",
    


]

// TODO: filter zero values on pi
// TODO: pressure based color of value?
const ZoomButtonGroup = props => {
    const { onClick } = props;

    return (
        <div id="zoom-button-group">
            <Button id="zoom-plus" onClick={e => onClick(e, 1)}>
                <Icon name="plus" />
            </Button>

            <Button id="zoom-minus" onClick={e => onClick(e, -1)}>
                <Icon name="minus" />
            </Button>
        </div>
    );
};
class TableView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            dataf: [],
            nodes: [],
            showModal: false,
            layout: {},
            zoom: 30,
            notified: false,
            selectedNode: "",
            columns: [
                {
                    Header: "nodeID",
                    accessor: "nodeID",
                    getProps: rowInfo => ({
                        style: {
                            backgroundColor: "white" //'#474b4f'
                        }
                    })
                },
                {
                    Header: "pressure (mPa)",

                    accessor: "value",
                    getProps: rowInfo => ({
                        style: {
                            backgroundColor: "white" //'#474b4f'
                        }
                    })
                },
                {
                    Header: "latitude",
                    accessor: "lat",
                    getProps: rowInfo => ({
                        style: {
                            backgroundColor: "white" //'#474b4f'
                        }
                    })
                },
                {
                    Header: "longitude",
                    accessor: "lon",
                    getProps: rowInfo => ({
                        style: {
                            backgroundColor: "white" //'#474b4f'
                        }
                    })
                },

                {
                    Header: "battery (%)",
                    accessor: "battery",
                    getProps: rowInfo => ({
                        style: {
                            backgroundColor: "white" //'#474b4f'
                        }
                    })
                },

                {
                    Header: "timestamp",
                    accessor: "timestamp",
                    getProps: rowInfo => ({
                        style: {
                            backgroundColor: "white" //'#474b4f'
                        }
                    })
                }
            ]
        };
    }

    async componentDidMount() {
        await this.refreshData();
    }

    handleFilterChange(event, data) {
        this.setState({
            dataf: this.state.data.filter(
                e => data["value"].indexOf(e["nodeID"]) > -1
            )
        });
    }

    handleOpenModal = (event, data) => {
        
        this.setState({
            showModal: true,
            selectedNode: data.value,
            dataForSelectedNode: this.getDataForSelectedNode(data.value),
            layout: {
                title: `<b>${data.value}</b>`,
                titlefont: {
                    size: 30
                },
                font: {
                    size: 16,
                    color: "black",
                    family: "IBM-Plex-Sans"
                },
                xaxis: {
                    tickangle: 35,
                    showticklabels: true,
                    type: "category"
                },

                yaxis: {
                    title: "Pressure (mPa)"
                },
                margin: {
                    t: 50, //top margin
                    l: 50, //left margin
                    r: 120, //right margin
                    b: 100 //bottom margin
                }
            }
        });
        
    };

    handleCloseModal = () => {
        this.setState({ showModal: false });
    };

    downloadData = () => {
        var csv = "# nodeID, value, latitude, longitude, battery, timestamp\n";
        csv += this.state.data
            .map(
                x =>
                    `${x.nodeID}, ${x.value}, ${x.lat}, ${x.lon}, ${x.battery}, ${
                        x.timestamp
                    }`
            )
            .join("\n");
        var blob = new Blob([csv], { type: "text/csv;charset=utf-8" });
        saveAs(blob, "data.csv");
    };

    refreshDataAndReloadTable = async () => {
        await this.refreshData();
        let selected = _.uniq(this.state.dataf.map(x => x.nodeID));
        this.setState({
            dataf: this.state.data.filter(
                e => selected.indexOf(e["nodeID"]) > -1
            )
        });
    };
    refreshData = () => {
        const options = {
            method: "GET",
            headers: {
                "Access-Control-Request-Headers": "GET",
                "Access-Control-Request-Method": "GET",
                "Authorization": 'Basic '+btoa('linde:Linde1234')
            }
        };

        fetch("https://api.snapmeter.io/logs", { options })
            .then(res => res.json())
            .then(json => {
                // Transform the data to display
                // 1. make timestamp more readable
                // 2. add a camera emoji the the ids associated with raspberry pif
                this.setState(prevState => {
                    var newData = [...json];
                    newData.map(x => {
                        x.timestamp =
                            x.timestamp
                                .substring(0, 10)
                                .split("-")
                                .reverse()
                                .join("/") +
                            " " +
                            x.timestamp.substring(11, 19);
                    });


                    var nodes = _.uniq(newData.map(x => x.nodeID));

                    const beforeTime = moment('07:00:00', 'HH:mm:ss')
                    const afterTime = moment('18:00:00', 'HH:mm:ss');
                    newData = newData.filter(x => {

                        /*if (CURRENT_IDS.includes(x.nodeID)) {
                            return true;
                        }*/
                    
                        if (PI_IDS.includes(x.nodeID)) {
                           
                            return moment(x.timestamp.split(' ')[1], 'HH:mm:ss').isBetween(beforeTime,afterTime)
                        }
                        return true;
                    });

     
                    const requireAttention = this.getDevicesRequiringAttention(newData, nodes);
                    const requireAttentionIDs = requireAttention.map(x => x.nodeID)
              
                    newData.map(x => {
                        var stripped = x.nodeID.split(' ')[0]
                        x.nodeID += requireAttentionIDs.indexOf(stripped) > -1 ? " ⚠️" : "";
                        x.nodeID += PI_IDS.indexOf(stripped) > -1 ? "📷" : "";
                    });

                    nodes = _.uniq(newData.map(x => x.nodeID));
                    nodes.sort();

                    if (requireAttention.length > 0) this.notify(requireAttention.length);

                    return {
                        data: newData,
                        nodeIDs: nodes,
                        requireAttention: requireAttention,
                        nodes: nodes.map(x => ({
                            key: x,
                            text: x,
                            value: x,
                            selected: true
                        }))
                    };
                });
            });
    };

    getDataForSelectedNode = (node, zoom = -1) => {
        // make sure no out of bounds slicing happening
        let currentZoom = zoom > 0 ? zoom : this.state.zoom;
   
        var values = this.state.dataf
            .filter(x => x.nodeID === node)
            .slice(0)
            .reverse();

  
        var valuesWindowed = values.slice(-zoom);


        return [
            {
                x: valuesWindowed.map(x => x.timestamp),
                y: valuesWindowed.map(x => x.value),
                marker: { color: "#1c92d2" }
            }
        ];
    };

    zoomHandler = (e, param) => {
        // check zoom out greater than data length
        var newZoom = this.state.zoom;
        if (param > 0) {
            newZoom -= newZoom - 5 < 5 ? 0 : 5;
        } else {
            newZoom +=
                newZoom >
                this.state.data.filter(
                    x => x.nodeID === this.state.selectedNode
                ).length
                    ? 0
                    : 5;
        }

        this.setState({
            zoom: newZoom,
            dataForSelectedNode: this.getDataForSelectedNode(
                this.state.selectedNode,
                newZoom
            )
        });

       
    };

    getDevicesRequiringAttention = (data, nodeIDs) => {
        //const dataReversed = data.reverse()

        var requireAttention = []

        nodeIDs.forEach(node => {
            var latest = data.find(x => x.nodeID === node)
            //console.log('DIFF ', moment().diff(moment.tz(latest.timestamp,'DD-MM-YYYY HH:mm:ss', 'Asia/Shanghai'), 'hours'))
            if ( latest.value > 300 || moment().diff(moment.tz(latest.timestamp,'DD-MM-YYYY HH:mm:ss', 'Asia/Shanghai'), 'hours') > 48 || latest.lon === 0) {
                requireAttention.push(latest)
            }

        })

        return(requireAttention)

    }

    notify = (count) =>  {

        toast.error(`${count} devices require your attention!`, {position: toast.POSITION.TOP_RIGHT, pauseOnHover: false});
    }

    render() {
        if (!this.state.nodes.length) {
            return (
                <div id="loader">
                    <Loader
                        type="Audio"
                        color="rgb(78, 64, 64)"
                        height="400"
                        width="400"
                    />
                </div>
            );
        } else {
            
  
            const padding = 90; // adjust this to your needs
            let height = 450 + padding;
            let heightPx = height + "px";
            let heightOffset = height / 2;
            let offsetPx = heightOffset + "px";

            const style = {
                content: {
                    border: "0",
                    borderRadius: "4px",
                    bottom: "auto",
                    height: heightPx, // set height
                    left: "50%",
                    padding: "2rem",
                    position: "fixed",
                    right: "auto",
                    top: "50%", // start from center
                    transform: "translate(-50%,-" + offsetPx + ")", // adjust top "up" based on height
                    width: "100%",
                    maxWidth: "60rem"
                }
            };

            return (
                <div id="table-div">
                    <ToastContainer autoClose={3000}  />
                    <ReactModal
                        style={style}
                        isOpen={this.state.showModal}
                        onRequestClose={this.handleCloseModal}
                        shouldCloseOnEsc={true}
                        shouldCloseOnOverlayClick={true}
                        ariaHideApp={false}
                    >
                        <Plot
                            data={this.state.dataForSelectedNode}
                            layout={this.state.layout}
                        />
                        <ZoomButtonGroup
                            onClick={(e, param) => this.zoomHandler(e, param)}
                        />
                    </ReactModal>

               
                    <Dropdown
                        id="node-filter"
                        multiple
                        selection
                        placeholder="Add Nodes"
                        options={this.state.nodes}
                        onLabelClick={(event, data) =>
                            this.handleOpenModal(event, data)
                        }
                        onChange={(event, data) =>
                            this.handleFilterChange(event, data)
                        }
                    />

                    <ReactTable
                        className="-striped -highlight"
                        pageSizeOptions={[10, 15, 20, 30, 50, 100]}
                        defaultPageSize={15}
                        data={this.state.dataf}
                        columns={this.state.columns}
                        NoDataComponent={() => <div> </div>}
                    />

                    <div id="button-div">
                        <Button id="download-btn" onClick={this.downloadData}>
                            <Button.Content visible>
                                <Icon name="download" />
                            </Button.Content>
                        </Button>

                        <Button
                            id="refresh-btn"
                            onClick={this.refreshDataAndReloadTable}
                        >
                            <Button.Content visible>
                                <Icon name="refresh" />
                            </Button.Content>
                        </Button>
                    </div>
                </div>
            );
        }
    }
}

export default TableView;
