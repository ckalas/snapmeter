import React from "react";
import Loader from "react-loader-spinner";
import MapGL, { Marker, Popup} from "react-map-gl";
import CylinderPin from "./CylinderPin";
import CylinderPopup from "./CylinderPopup";

// TODO: transform the values in database you moron
// TODO: color the pin based on value

export default class MapView extends React.Component {
    // todo: get data and nodes
    constructor(props, context) {
        super(props);
        this.state = {
            data: [],
            nodes: [],
            clusters: [],
            popupInfo: null,
            viewport: {
                width: window.innerWidth * 0.97,
                height: window.innerHeight * 0.85,
                latitude: 31.299,
                longitude: 120.5853,
                zoom: 8
            }
        };
    }

    componentDidMount() {
        var options = {
            method: "GET",
            headers: {
                "Access-Control-Request-Headers": "GET",
                "Access-Control-Request-Method": "GET",
                "Authorization": 'Basic '+btoa('linde:Linde1234')
            }
        };

        fetch("https://api.snapmeter.io/locations", { options })
            .then(res => res.json())
            .then(json => {
                this.setState({
                    nodes: json.map(x => ({
                        nodeID: x.name,
                        lat: x.location.coordinates[1],
                        lng: x.location.coordinates[0]
                    }))
                });
            });
        fetch("https://api.snapmeter.io/logs/latest_each", { options })
            .then(res => res.json())

            .then(json => {
                this.setState({
                    data: json.map(x => ({
                        nodeID: x.nodeID,
                        value: x.latestValue,
                        timestamp: x.timestamp,
                        battery: x.battery
                    }))
                });
            });
    }

    _renderPopup() {
        const { popupInfo, data } = this.state;

        return (
            popupInfo && (
                <Popup
                    tipSize={5}
                    anchor="top"
                    longitude={popupInfo.lng}
                    latitude={popupInfo.lat}
                    onClose={() => this.setState({ popupInfo: null })}
                >
                    <CylinderPopup
                        info={
                            data.filter(x => x.nodeID === popupInfo.nodeID)[0]
                        }
                    />
                </Popup>
            )
        );
    }

    _renderMarker = x => {
        const {data} = this.state
        return (
            <Marker
                key={`marker-${x.nodeID}`}
                longitude={x.lng}
                latitude={x.lat}
            >
                <CylinderPin
                    size={30}
                    value={data.filter(node => node.nodeID === x.nodeID)[0].value}
                    timestamp={data.filter(node => node.nodeID === x.nodeID)[0].timestamp}
                    onClick={() => this.setState({ popupInfo: x })}
                />
            </Marker>
        );
    };

    render() {
        if (!this.state.data.length) {
            return (
                <div id="loader">
                    <Loader
                        type="Audio"
                        color="rgb(78, 64, 64)"
                        height="400"
                        width="400"
                    />
                </div>
            );
        } else {
            return (
                    <MapGL
                        mapStyle="mapbox://styles/mapbox/light-v9"
                        {...this.state.viewport}
                        onViewportChange={viewport =>
                            this.setState({ viewport })
                        }
                        mapboxApiAccessToken="pk.eyJ1IjoiY2thbGFzIiwiYSI6ImNqajlwbG03OTFheGszdm81b2JhYWI1M3IifQ.kIgUNRhIA4tWrqbyulVnfg"
                    >
                        {this.state.nodes.map(this._renderMarker)}
                        {this._renderPopup()}
                    </MapGL>
            );
        }
    }
}
