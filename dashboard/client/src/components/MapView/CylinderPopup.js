import React, { PureComponent } from "react";
import { Line } from "rc-progress";

const MAX_MPA = 22;
// TODO: add in logic for last reading
const colorFromValue = value => {
    if (value <= 5) {
        return "red";
    } else if (value <= 10) {
        return "yellow";
    } else if (value <= 25) {
        return "#63ff00";
    } else {
        return "#FF69B4";
    }
};

export default class CylinderPopup extends PureComponent {
    render() {
        const { info } = this.props;

        return (
            <div className="cylinder-popup">
                <h2>{info.nodeID}</h2>
                <Line
                    percent={(info.value / MAX_MPA) * 100}
                    strokeColor={colorFromValue(info.value)}
                    strokeWidth="3"
                    style={{ width: "200px" }}
                />
                <h3>{`${info.value} mPa`}</h3>

                <p>
                    {" "}
                    <span role="img">⚡️ </span>
                    {info.battery} %
                </p>
                
                <p>
                    {" "}
                    <span role="img">📅 </span>
                    {info.timestamp
                        .substring(0, 10)
                        .split("-")
                        .reverse()
                        .join("/") +
                        " " +
                        info.timestamp.substring(11, 19)}
                </p>

            </div>
        );
    }
}
