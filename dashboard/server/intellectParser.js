const fs = require('fs')
const request = require('request');
var parseString = require('xml2js').parseString;
const moment = require('moment-timezone')

const mongoose = require('mongoose');
const config = require('./config/config');
const Log = require('./models/Log');
const Location = require('./models/Location');
const _ = require('lodash')

/*
// Set up Mongoose
mongoose.connect(config.db, (err) => {
  if (err) throw err;
})
  .then(() => {
    console.log("Connected to database")
});
mongoose.Promise = global.Promise;*/
const PI_IDS = [
    "0x00072beb",
    "0x00072db2",
    "0x00072563",
    "0x00072859"
];

function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

ck_lat = /^(-?[1-8]?\d(?:\.\d{1,18})?|90(?:\.0{1,18})?)$/;
ck_lon = /^(-?(?:1[0-7]|[1-9])?\d(?:\.\d{1,18})?|180(?:\.0{1,18})?)$/;

function check_lat_lon(lat, lon){
  var validLat = ck_lat.test(lat);
  var validLon = ck_lon.test(lon);
  if(validLat && validLon) {
      return true;
  } else {
      return false;
  }
}

// TODO: store last message id and use that optionally in query
var intellectParser =  () => {

    const username = 'chris.kalas@linde.com';
    const password =  'Chris123456!';
    const host = 'intellect.mctiot.cn';
    const dataURL = "/data/v1/receive";
    const tokenURL = "/config/v1/session";
    var sduArray = [];
    var lastMessageID = ""

    getLastSDU = () => {

        fs.readFile('lastMessageID.json', (error, data) => {
            try {
                console.log('READING SDU ID', JSON.parse(data).lastSDUID)

                lastMessageID = JSON.parse(data).lastSDUID
            }
            catch (e){
                console.log("Error reading last message ID")
            }

        })
    }

    getSites = () => {
        const content = fs.readFileSync("sites.json", "utf-8");   
        const sites = JSON.parse(content).sites; 
        var siteList = [];  
        sites.forEach((site,n) => { siteList.push(site) });
        return siteList;

    }

    writeLastSDU = (sdu) => {
        console.log(JSON.stringify({'lastSDUID': sdu.messageID}))
        fs.writeFile('lastMessageID.json', JSON.stringify({'lastSDUID': sdu.messageID}), (err) => {
            
        })
    }

    distance = (lat1,lon1,lat2,lon2) => {
        const R = 6371; // Earth's radius in Km
        return Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
                        Math.cos(lat1)*Math.cos(lat2) *
                        Math.cos(lon2-lon1)) * R;
    }

    getSDUInfo = () => {

        sduArray.forEach((sdu) => {
      
            let localRxTime = moment.tz(sdu.timestamp, 'Asia/Shanghai')
            console.log(localRxTime, localRxTime.utc(true))
            // get battery reading, bytes 22-25 as hex * 10
            let battery = parseInt(sdu.payload.slice(22,26),16)/10 
            console.log('battery reading ', battery)
            // parse gps +  timestamp
            var rlat, rlng = 0
            if (sdu.payload.substring(0,2) === '0D') {
                console.log('No GPS')
            }
            else {
                let lat= parseInt(sdu.payload.substring(7,14),16)
            	let lng= parseInt(sdu.payload.substring(14,22),16)
            	rlat= parseFloat((lat/1000000.000000)).toFixed(6)
            	rlng= parseFloat((lng/1000000.000000)).toFixed(6)
            }

            console.log(rlat, rlng)

            var value = -1;

            let pressureGaugeReading = sdu.payload.match(/0D010302(\w{4})/)

            if (pressureGaugeReading !== null) {
              console.log(sdu.payload.substring(pressureGaugeReading[1]))
		      value = parseInt(pressureGaugeReading[1], 16) / 2000.0 * 30
              console.log(`Pressure gauge reading: ${value} Mpa`)
            }
             
            else {

                var decoded = hex2a(sdu.payload)
                if (!decoded.length) return;
                var readings = decoded.match(/\d{1,}\.\d{1,} (\d{1,})/g)
                if (!readings) return;
                var values = []
                readings.forEach(x => {
                    console.log(x, x.split(' '))
                    values.push(Number(x.split(' ')[1]))
                })
                values.sort((a, b) => a - b);
                let median = (values[(values.length - 1) >> 1] + values[values.length >> 1]) / 2
                //value = PI_IDS.indexOf(sdu.nodeID) > -1 ? median < 135 ? 0 : (1.49 * (median - 90) - 64.28).toFixed(2): median;
                value = median < 135 ? 0 : (1.49 * (median - 90) - 64.28) / 10;
  
            }
            
            var logQuery = {'messageID' : sdu.messageID};

            var logDocument = {
                                'messageID': sdu.messageID,
                                'nodeID': sdu.nodeID,
                                'reading': {
                                            'timestamp': localRxTime,
                                            'lat': rlat,
                                            'lon': rlng,
                                            'value': Number(value.toFixed(4)),
                                            battery
                                }
            }

            Log.findOneAndUpdate(logQuery, logDocument, {upsert:true}, function(err, doc){
                if (err) return console.log('Error saving to db ', err);
                return console.log("Succesfully saved logs");
            });
            
            if (check_lat_lon(rlat, rlng)) {

                const sites = getSites()
                var onsite = false;
                // assume that the only one site is within distance
                // if performance is needed change to loop so can break
                sites.forEach(site => {
                    console.log(distance(site.lat, site.lon, rlat, rlng))
                    if (distance(site.lat, site.lon, rlat, rlng) < 10) {
                        console.log(`${sdu.nodeID} is at ${site.name}`)
                        onsite = true;
                    }
                })
                
                // for each entry, check if distance is than something
                // i dont get the unit of distance btw, supposedly km but i'm doubtful

                var locationQuery = {'name' : sdu.nodeID};

                var locationDocument = { 
                        'name': sdu.nodeID,
                        'category': 'node',
                        'onsite': onsite,
                        'location': {
                                    'type':'Point',
                                    'coordinates': [rlng, rlat]
                        }
                }
    
        

                Location.findOneAndUpdate(locationQuery, locationDocument, {upsert:true}, function(err, doc){
                    if (err) return console.log('Error saving to db ', err);
                    return console.log("Succesfully saved location");
                });
            }


        });
    }

    getData =  (token) => {
            
        request( {
            method: 'GET',
            uri: lastMessageID.length ? `https://${host}${dataURL}/${lastMessageID}?count=500` : 
                                        `https://${host}${dataURL}`,
            headers: {'Authorization': token, 'Accept': 'application/xml'}
        },
        (error, response, body) => {

            if (error) {
                console.log('Error connecting')
            }
            else {

                processData(body)
                if (sduArray.length) {
                    writeLastSDU(_.last(sduArray))
                    getSDUInfo(sduArray)
                }
                else {
                    console.log('No new readings')
                }
                console.log('Done')


            }
        });

    }

    processData = (data) => {
        parseString(data, (err, result) => {

            if ('uplink' in result.uplinks) {
                sduArray = []
                result.uplinks.uplink.forEach(x => {
                    const uplink = x.datagramUplinkEvent[0]
    
                    sduArray.push({messageID: x.messageId[0],
                                payload: uplink.payload[0],
                                timestamp: uplink.timestamp[0],
                                nodeID: uplink.nodeId[0]

                                }
                    )
                });
            }

        });
    }


    return {
        // request auth token
        fetch: () => {
            getLastSDU()
            request( {
                method: 'POST',
                uri: `https://${host}${tokenURL}`,
                headers: {username, password}
            },
            (error, response, body) => {
    
                if (error) {
                    console.log('Error connecting')
                }
                else {
                    
                    // request the data
                    getData(JSON.parse(body).token)
    
    
                }
            });
        }


    }

};


module.exports = {
    intellectParser: intellectParser()
}

