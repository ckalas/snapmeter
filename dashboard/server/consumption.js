db.getCollection('logs').aggregate([
                        { 
                        $match : {
                                "reading.timestamp": {$gt: new Date().getTime()-(1*60*60*1000) }
                            }
                          
                       },

                       {
                       $group:
                             {
                             _id: "$nodeID",
                             first: { $first: "$reading.value"},
                             last: { $last: "$reading.value"},
                             date: {$max: "$reading.timestamp"}

                             }
                       }, 

                       {
                         $project: {
                                date:  {$dateToString: { format: "%G-%d-%m", date: "$date"}},
                                consumption: { $subtract :  ["$first","$last"]} 
                             }
                       }
      
                   
                    ])