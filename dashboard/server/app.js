var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
var helmet = require("helmet");
const basicAuth = require('express-basic-auth')

const mongoose = require("mongoose");
const config = require("./config/config");

const parser = require("./intellectParser");
const intellectParser = parser.intellectParser;

// Set up Mongoose
mongoose
  .connect(
    config.db,
    err => {
      if (err) throw err;
    }
  )
  .then(() => {
    console.log("Connected to database");
  });

mongoose.Promise = global.Promise;

const app = express();

app.use(cors());



app.use(helmet());
app.use(express.static("static"));

// API routes
require("./routes")(app);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});



// when first launch update the db
intellectParser.fetch();
// update every half an hour
setInterval(intellectParser.fetch, 1800000);

module.exports = app;
