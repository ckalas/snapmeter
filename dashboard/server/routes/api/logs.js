const Log = require('../../models/Log');

module.exports = (app) => {

    app.get('/idiot', (req, res, next) => {
        Log.find({})
          .exec()
          .then((log) => {
              console.log(log)
              res.json(log)
          })
          .catch((err) => next(err));
    });

    app.get('/details/*', (req,res,next) => {
        Log.find({nodeID: req.params['0']})
        .exec()
        .then((log) => res.json(log))
        .catch((err)=> next(err));
    });

    app.get('/logs/devices', (req,res,next) => {
        Log.aggregate(
            [
                //{ $match: { "reading.lon": {$ne:-1}}},
                {
                    $group : 
                        { 
                            _id: {"nodeID": "$nodeID"}
    
                        }
                },
                
                {
                $project:
                    {
                        "_id" : 0,
                        "nodeID": "$_id.nodeID",
    
                }
                }
            ]
    ) 
    .exec()
    .then((log) => res.json(log))
    .catch((err) => next(err));
  });

  app.get('/logs/consumption', (req,res,next) => {
    Log.aggregate(
        [
            { 
            $match : {
                    "reading.timestamp": {$gt: new Date().getTime()-(1*60*60*1000) }
                }
              
           },

           {
           $group:
                 {
                 _id: "$nodeID",
                 first: { $first: "$reading.value"},
                 last: { $last: "$reading.value"},
                 date: {$max: "$reading.timestamp"}

                 }
           }, 

           {
             $project: {
                    date:  {$dateToString: { format: "%G-%d-%m", date: "$date"}},
                    consumption: {$multiply:[{$divide:[{ $subtract :  ["$first","$last"]} ,20.0]},100]} 
                 }
           }

       
        ]
) 
.exec()
.then((log) => res.json(log))
.catch((err) => next(err));
});

  app.get('/logs/devices/count', (req,res,next) => {
    Log.aggregate(
        [
            {
                $group: 
                { 
                    _id: {"nodeID": "$nodeID"},
    

                }
              },
              { $count: "numberOfDevices"
              }
        ]
    ) 
    .exec()
    .then((log) => res.json(log))
    .catch((err) => next(err));
    });
  

    app.get('/logs', (req, res, next) => {
        Log.aggregate(
          [
              { $sort: { "reading.timestamp": -1}},
              /*{ $match: { "reading.lon" : {"$ne": -1}}},*/            
              
              {
                  $project:
                      {
                          "_id" : 0,
                          "nodeID": "$nodeID",
                          "lat": "$reading.lat",
                          "lon": "$reading.lon",
                          "value": "$reading.value",
                          'timestamp': "$reading.timestamp",
                          "battery": "$reading.battery"
  
                      }
              }
              
              
           
          ]
        )
        .exec()
        .then((log) => res.json(log))
        .catch((err) => next(err));
      });
  



    app.get('/logs/latest_each', (req, res, next) => { 
      Log.aggregate(
        [
            { $sort: { "reading.timestamp": -1}},
            {
              $group :
                    {
                        _id: "$nodeID",
                        latestValue: {$first: "$reading.value"},
                        timestamp: {$first: "$reading.timestamp"},
                        battery: {$first: "$reading.battery"}
                    }
            },

            {
                $project:
                    {
                        _id: 0,
                        nodeID: "$_id",
                        latestValue: "$latestValue",
                        timestamp: "$timestamp",
                        battery: "$battery"

                    }
            }
            
          
        ]
      ) 
      .exec()
        .then((log) => res.json(log))
        .catch((err) => next(err));
      });
  
}
