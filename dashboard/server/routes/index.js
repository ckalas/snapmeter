var express = require('express');
const fs = require('fs');

module.exports = (app) => {
  // API routes
  fs.readdirSync(__dirname + '/api/').forEach((file) => {
    if (file[0] !== '.') {

      require(`./api/${file.substr(0, file.indexOf('.'))}`)(app);
    }
  });
};
