// get the max value for each node each day
db.logs.aggregate(
        [
            {
                $group : 
                    { 
                        _id: {"nodeID": "$nodeID", "date": "$reading.timestamp"},
                        maxValue: {$max: "$reading.value"}
                    }
            }
        ]
);

// get the most recent value and time stamp for each node
db.logs.aggregate(
        [
            { $sort: { "reading.timestamp": -1}},
            {
              $group :
                    {
                        _id: "$nodeID",
                        latestValue: {$first: "$reading.value"},
                        timestamp: {$first: "$reading.timestamp"}
                    }
            }
         
        ]
);

// get the most recent document for each node
db.logs.aggregate(
        [
            { $sort: { "reading.timestamp": -1}},
            {
              $group :
                    {
                        _id: "$nodeID",
                        features: {$push: "$$ROOT"}
                    }
            },
            
            {
                $replaceRoot :
                    {
                        newRoot: {$arrayElemAt: ["$features", 0]}
                    }
            }
            
            
         
        ]
);


// get array of values in order for each node
db.logs.aggregate(
        [
            {$match: {}},
            { $group: {_id: "$nodeID", value: {$push:"$reading.value"}, timestamp: {$push:"$reading.timestamp"}}}

        ]
);


// get array of values in order for each node greater than 0
db.logs.aggregate(
        [
            {$match: {"reading.value": {$ne: 0}}},
            { $group: {_id: "$nodeID", value: {$push:"$reading.value"}, timestamp: {$push:"$reading.timestamp"}}},

        ]
);

// get the average over each time window for each node
db.logs.aggregate(
        [
            {$match: {}},
            {
                $group : 
                    { 
                        _id: {"nodeID": "$nodeID", "date": "$reading.timestamp"},
                        avg: {$avg: "$reading.value"}
                    }
            },
            { $group :
                {
                    _id :"$_id.nodeID",
                    res: {$push:"$avg"}
                }
                
                
            }
            
        ]
);

// latest positon not eq -1
db.getCollection('logs').aggregate(
    [
        { $sort: { "reading.timestamp": -1}},
        { $match: { "reading.lon" : {"$ne": -1}}},
        {
          $group :
                {
                    _id: "$nodeID",
                    features: {$push: "$$ROOT"}
                }
        },
        
        {
            $replaceRoot :
                {
                    newRoot: {$arrayElemAt: ["$features", 0]}
                }
        },
        
        {
            $project:
                {
                    "_id" : 0,
                    "nodeID": "$nodeID",
                    "lat": "$reading.lat",
                    "lon": "$reading.lon",
                    "value": "$reading.value"
                }
        }
        
        
     
    ]
)


db.logs.aggregate(
    [
        {
            $group : 
                { 
                    _id: {"nodeID": "$nodeID"}

                }
        },
        
        {
        $project:
            {
                "_id" : 0,
                "nodeID": "$_id.nodeID",

        }
        }
    ]
);

