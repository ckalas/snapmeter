#!/bin/sh
echo "**************** starting setup of system for opencv ************************"
sudo apt-get update && sudo apt-get -y upgrade 
sudo apt-get -y install python-dev python-skimage python-numpy python-pip
#sudo pip install matplotlib picamera
# We need to install some packages that allow OpenCV to process images:
sudo apt-get -y install libtiff5-dev libjasper-dev libpng12-dev
# If you get an error about libjpeg-dev try installing this first:
# sudo apt-get install libjpeg-dev
# We need to install some packages that allow OpenCV to process videos:
sudo apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
# We need to install the GTK library for some GUI stuff like viewing images.
sudo apt-get -y install libgtk2.0-dev
# We need to install some other packages for various operations in OpenCV:
sudo apt-get -y install libatlas-base-dev gfortran
# We need to install pip if you haven't done so in the past:


# Download and install the file from the repo called "latest-OpenCV.deb".
wget --no-check-certificate -N "https://github.com/jabelone/OpenCV-for-Pi/raw/master/latest-OpenCV.deb"
sudo dpkg -i latest-OpenCV.deb

ver=$(python -c '
import cv2
print cv2.__version__
')

if [ $ver = "3.1.0" ]
then
	echo "******************* opencv version" $ver "installed correctly ******************"
	rm latest-OpenCV.deb
else 
	echo "**************** opencv not installed correctly ************************"
fi

echo "**************** please run raspi-config and enable camera ************************"